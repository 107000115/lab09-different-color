const { ccclass, property } = cc._decorator;

@ccclass
export default class Menu extends cc.Component {

    // ===================== TODO =====================
    // 1. Add click event to StartButton to call this
    //    function
    start() {
        this.node.on(cc.Node.EventType.MOUSE_DOWN, function (event) {
            this.loadGameScene();
        }, this);
    }


    loadGameScene() {
        cc.director.loadScene("game");
    }
    // ================================================
}
